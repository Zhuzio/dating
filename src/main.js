// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { AjaxPlugin, LoadingPlugin  } from 'vux'
import interfaces from './assets/js/itf'
Vue.config.productionTip = false

Vue.use(AjaxPlugin)
    .use(LoadingPlugin)

Vue.prototype.api = interfaces
Vue.prototype.baseImgUrl = 'http://test.tanxinba.cn/'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

import Vue from 'vue'
import Router from 'vue-router'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '首页',
      component: resolve => require(['../pages/index.vue'], resolve)
    },
    {
      path: '/mine',
      name: '我的',
      component: resolve => require(['../pages/mine.vue'], resolve)
    },
    {
      path: '/marriageSeeking',
      name: '征婚',
      component: resolve => require(['../pages/marriageSeeking.vue'], resolve)
    },
    {
      path: '/teacher',
      name: '导师',
      component: resolve => require(['../pages/teacher.vue'], resolve)
    },
  ]
})

import Vue from 'vue';
import { AjaxPlugin } from 'vux'


Vue.use(AjaxPlugin)

const _this = Vue.prototype.$http;

// console.log(Vue.prototype.$http)
// console.log(_this)

_this.defaults.baseURL ="http://test.tanxinba.cn/";

export function API_POST(url, params) {
  return new Promise((resolve, reject) => {
    _this.post(url, params)
      .then(response => {
        if (response.status === 200 || response.status === 500 || response.status === 501) {
          resolve(response.data)
        } else {
          reject(response)
        }
      })
  })
}

export function API_GET(url, params) {
  return new Promise((resolve, reject) => {
    _this.get(url, qs.stringify(params))
      .then(response => {
        if (response.status === 200 || response.status === 500 || response.status === 501) {
          resolve(response.data)
        } else {
          reject(response)
        }
      })
  })
}

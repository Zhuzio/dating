import qs from 'qs'
const ug = function (params) {
  return qs.stringify(params)
}
import { API_POST, API_GET } from "./api";

export default {
  //  获得首页文章
  getIndexArticle(params) {
    return API_POST(`Article/index`, ug(params))
  },
  // 获得首页筛选条件
  getIndexSelectConditions(params) {
    return API_POST(`Article/category_list`, ug(params))
  }
}
